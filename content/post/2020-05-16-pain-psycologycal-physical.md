---
title: Physical pain and psychological pain
date: 2020-05-16
weight: 10
---
Le pire types de souffrance est psychologique.
La douleur physique est beaucoup plus visible. 

On l'entend dans cette [interview de Elizabeth Gilbert](https://tim.blog/2020/05/08/elizabeth-gilbert/):

> Une fracture met 6 semaines à se réparer,  alors que un mot peut causer des dégâts pendant 40 ans.

Dans l'interview d’un nord-coréen, il raconte que sa souffrance n'est pas de se faire battre mais d'avoir gâché un repas pour 5 personnes.
{{< youtube DyqUw0WYwoc >}} 

